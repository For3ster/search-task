# Vivira | Search Tech Task

The decisions made in this project were driven by the original requirements.

There is much room for further potential improvement of the project:

**Technically:**
- multi-modularity
- complete navigation (using [Navigation component](https://developer.android.com/jetpack/compose/navigation) or such libraries as [Modo](https://github.com/terrakok/Modo))
- test improvement

**Functionally:**
- displaying additional information, such as the number of repository stars
- sorting and/or filters
- opening a separate page with a detailed description of the repository
- ability to add repositories to favorites
- full authorization and part of GitHub functionality, e.g. viewing your personal repositories
- and so on

### Tech info

  - [Kotlin](https://kotlinlang.org)
  - [Compose](https://developer.android.com/jetpack/compose)
  - MVVM Architecture
  - Min SDK 23

### Libraries Used

* [Hilt](https://developer.android.com/training/dependency-injection/hilt-android) for dependency injection
* [Paging](https://developer.android.com/topic/libraries/architecture/paging/v3-overview) for efficient pagination support
* [OkHttp](https://github.com/square/okhttp) and [Retrofit](https://github.com/square/retrofit) as an Http client
* [Moshi](https://github.com/square/moshi) for data parsing
* [Coil](https://coil-kt.github.io/coil/) for image loading
* [JUnit 5](https://github.com/junit-team/junit5) - testing framework
* [Kotest](https://kotest.io/) - testing framework
* [Mockito](https://site.mockito.org/) and [Mockito Kotlin](https://github.com/mockito/mockito-kotlin) as a mocking framework for unit tests
* [Turbine](https://github.com/cashapp/turbine) for testing coroutines Flow
