package vivira.search.data.api

import retrofit2.http.GET
import retrofit2.http.Query
import vivira.search.data.model.Repositories

interface SearchApi {

    @GET("repositories")
    suspend fun searchRepositories(
        @Query("q") query: String,
        @Query("page") page: Int = 1,
        @Query("per_page") itemsPerPage: Int = ITEMS_PER_PAGE,
    ): Repositories

    companion object {

        const val ITEMS_PER_PAGE = 30
    }
}
