package vivira.search.data.model

import com.squareup.moshi.Json

data class Repository(
    val id: Int,
    val name: String,
    @Json(name = "full_name") val fullName: String,
    val description: String?,
    @Json(name = "html_url") val htmlUrl: String,
    val language: String?,
    val owner: Owner,
)

data class Owner(
    val id: Int,
    val login: String,
    @Json(name = "avatar_url") val avatarUrl: String,
)

@JvmInline
value class Repositories(val items: List<Repository>)
