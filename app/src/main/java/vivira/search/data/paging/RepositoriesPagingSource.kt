package vivira.search.data.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import vivira.search.data.api.SearchApi
import vivira.search.data.model.Repository

class RepositoriesPagingSource(
    private val api: SearchApi,
    private val query: String,
) : PagingSource<Int, Repository>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Repository> =
        withContext(Dispatchers.IO) {
            if (query.isBlank()) {
                LoadResult.Page(
                    data = emptyList(),
                    prevKey = null,
                    nextKey = null
                )
            } else {
                try {
                    val page = params.key ?: 1
                    val items = api.searchRepositories(query = query, page = page).items

                    LoadResult.Page(
                        data = items,
                        prevKey = if (page == 1) null else page - 1,
                        nextKey = if (items.isEmpty()) null else page + 1,
                    )
                } catch (exception: Exception) {
                    LoadResult.Error(exception)
                }
            }
        }

    override fun getRefreshKey(state: PagingState<Int, Repository>): Int? =
        state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
}
