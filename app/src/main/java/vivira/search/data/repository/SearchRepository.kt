package vivira.search.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import vivira.search.data.api.SearchApi
import vivira.search.data.model.Repository
import vivira.search.data.paging.RepositoriesPagingSource
import javax.inject.Inject

class SearchRepository @Inject constructor(
    private val api: SearchApi,
) {

    fun search(query: String): Flow<PagingData<Repository>> =
        Pager(
            config = PagingConfig(
                pageSize = SearchApi.ITEMS_PER_PAGE,
                prefetchDistance = 2
            ),
            pagingSourceFactory = {
                RepositoriesPagingSource(api, query)
            }
        ).flow
}
