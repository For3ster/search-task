package vivira.search.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import vivira.search.data.api.SearchApi
import javax.inject.Singleton

private const val BASE_URL = "https://api.github.com/search/"

@Module
@InstallIn(SingletonComponent::class)
object SearchModule {

    @Provides
    @Singleton
    fun provideSearchApi(
        builder: Retrofit.Builder
    ): SearchApi =
        builder
            .baseUrl(BASE_URL)
            .build()
            .create(SearchApi::class.java)
}
