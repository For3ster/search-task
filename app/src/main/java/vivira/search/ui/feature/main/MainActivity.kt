package vivira.search.ui.feature.main

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import dagger.hilt.android.AndroidEntryPoint
import vivira.search.ui.feature.search.SearchScreen
import vivira.search.ui.theme.ViviraSearchTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ViviraSearchTheme {
                SearchScreen()
            }
        }
    }
}
