package vivira.search.ui.theme

import androidx.compose.ui.graphics.Color

val Primary = Color(0xFFFFE4BC)
val Secondary = Color(0xFFDCD0C2)
val Tertiary = Color(0xFFE4A889)

val PrimaryLight = Color(0xFFE7B06D)
val SecondaryLight = Color(0xFFC2AC93)
val TertiaryLight = Color(0xFFA56D52)
