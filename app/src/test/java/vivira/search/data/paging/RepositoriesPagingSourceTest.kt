package vivira.search.data.paging

import androidx.paging.PagingSource
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe
import org.mockito.kotlin.any
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.doThrow
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import vivira.search.data.api.SearchApi
import vivira.search.data.model.Repositories
import vivira.search.data.model.Repository
import kotlin.random.Random

/**
 * Something broke after the Studio update, so all mock frameworks do not work for me.
 * But this is what test implementation should look like for a paging source.
 */
class RepositoriesPagingSourceTest : BehaviorSpec({

    val api: SearchApi = mock()

    Given("Valid query") {
        whenever(api.searchRepositories(any())) doReturn REPOSITORIES
        val givenQuery = "test"
        val pagingSource = RepositoriesPagingSource(api, givenQuery)

        When("Load paging source data") {
            val params = PagingSource.LoadParams.Refresh(
                key = 1,
                loadSize = 30,
                placeholdersEnabled = false
            )
            val result = pagingSource.load(params)
            Then("Page should be returned") {
                val expectedResult = PagingSource.LoadResult.Page(
                    data = REPOSITORIES.items,
                    prevKey = null,
                    nextKey = 2
                )
                result shouldBe expectedResult
            }
        }

        When("Load next paging data") {
            val params = PagingSource.LoadParams.Append(
                key = 2,
                loadSize = 30,
                placeholdersEnabled = false
            )
            val result = pagingSource.load(params)
            Then("Next page should be returned") {
                val expectedResult = PagingSource.LoadResult.Page(
                    data = REPOSITORIES.items,
                    prevKey = 1,
                    nextKey = 3
                )
                result shouldBe expectedResult
            }
        }
    }

    Given("Empty query") {
        whenever(api.searchRepositories(any())) doReturn Repositories(emptyList())
        val givenQuery = ""
        val pagingSource = RepositoriesPagingSource(api, givenQuery)
        val params = PagingSource.LoadParams.Refresh(
            key = 1,
            loadSize = 30,
            placeholdersEnabled = false
        )

        When("Load paging source data") {
            val result = pagingSource.load(params)
            Then("Page with empty data should be returned") {
                val expectedResult = PagingSource.LoadResult.Page(
                    data = emptyList(),
                    prevKey = null,
                    nextKey = null
                )
                result shouldBe expectedResult
            }
        }
    }

    Given("Unexpected error") {
        whenever(api.searchRepositories(any())) doThrow RuntimeException()
        val givenQuery = "test"
        val pagingSource = RepositoriesPagingSource(api, givenQuery)
        val params = PagingSource.LoadParams.Refresh(
            key = 1,
            loadSize = 30,
            placeholdersEnabled = false
        )

        When("Load paging source data") {
            val result = pagingSource.load(params)
            Then("Error result should be returned") {
                val expectedResult = PagingSource.LoadResult.Error<Int, Repository>(
                    throwable = RuntimeException()
                )
                result shouldBe expectedResult
            }
        }
    }
}) {

    private companion object {

        val REPOSITORY: Repository = mock {
            on { id } doAnswer { Random.nextInt() }
        }
        val REPOSITORIES = Repositories(List(30) { REPOSITORY })
    }
}
