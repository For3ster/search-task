package vivira.search.ui.feature.search

import app.cash.turbine.turbineScope
import io.kotest.core.coroutines.backgroundScope
import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.string.shouldBeEmpty
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.mockito.kotlin.mock
import vivira.search.data.api.SearchApi
import vivira.search.data.repository.SearchRepository

@OptIn(ExperimentalCoroutinesApi::class)
class SearchViewModelTest : FunSpec({

    val api: SearchApi = mock()
    val repository = SearchRepository(api)
    val viewModel = SearchViewModel(repository)

    test("Search text default value should be an empty string") {
        turbineScope {
            val receiver = viewModel.searchText.testIn(backgroundScope)
            receiver.awaitItem().shouldBeEmpty()
            receiver.cancel()
        }
    }

}) {
    init {
        coroutineTestScope = true
    }

    @AnnotationSpec.Before
    fun setUp() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
    }

    @AnnotationSpec.After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}
